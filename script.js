function OpenMOT() { document.getElementById("MOT").classList.add("rotate-box") }

function CloseMOT() { document.getElementById("MOT").classList.remove("rotate-box") }

var slideIndex = 1;
showSlides(slideIndex);
function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("data-for-thumbnail");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none"
    }
    slides[slideIndex - 1].style.display = "block"
}

var carousel = document.querySelector(".people");
var cellCount = 8;
var selectedIndex = 0;
function getCarousel() {
    var angle = selectedIndex / cellCount * -360;
    carousel.style.transform = "rotateY(" + angle + "deg)"
}

function prev(n) {
    showSlides(slideIndex += n);
    selectedIndex--;
    getCarousel()
}

function next(n) {
    showSlides(slideIndex += n);
    selectedIndex++;
    getCarousel()
}